export interface MedicalAppointment {
  id: number;
  specialty: string;
  date: string;
}
