export interface ReceiptDetail {
  id: number;
  details: string;
}
