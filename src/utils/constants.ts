export const citasMedicasProps = [
  'REHABILITACIÓN',
  'MEDICINA GENERAL',
  'ESTOMATOLOGÍA',
  'GINECOLOGÍA',
] as const;

export const api = 'https://apilex.herokuapp.com';
