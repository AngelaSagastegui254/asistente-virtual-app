export interface BotData {
  botId: string;
  botAliasId: string;
}

export const botRegion = 'us-east-1';

export const authBotData: BotData = {
  botId: 'UAJBDNEPJV',
  botAliasId: 'TSTALIASID',
};

export const medicalApointmentBotData: BotData = {
  botId: 'HO8CSVXGIH',
  botAliasId: 'TSTALIASID',
};

export const medicalReceiptBotData: BotData = {
  botId: 'LVDTGE8QRH',
  botAliasId: 'TSTALIASID',
};
