import {CommonActions} from '@react-navigation/native';

export function resetAction(routeName: string, params?: any) {
  return CommonActions.reset({
    index: 0,
    routes: [
      {
        name: routeName,
        params,
      },
    ],
  });
}
