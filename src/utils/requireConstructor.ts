export const requireConstructor = (imageSrc: string): string => {
  let logo = '';
  switch (imageSrc) {
    case 'calendar':
      logo = require('../assets/img/calendar.png');
      break;
    case 'exit':
      logo = require('../assets/img/exit.png');
      break;
    case 'observations':
      logo = require('../assets/img/observations.png');
      break;
    case 'syringe':
      logo = require('../assets/img/syringe.png');
      break;
    case 'band-aid':
      logo = require('../assets/img/band-aid.png');
      break;
    case 'doctor':
      logo = require('../assets/img/doctor.png');
      break;
    case 'kit':
      logo = require('../assets/img/kit.png');
      break;
    case 'questionnaire':
      logo = require('../assets/img/questionnaire.png');
      break;
  }

  return logo;
};
