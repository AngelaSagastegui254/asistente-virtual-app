import {Dispatch, SetStateAction, useEffect} from 'react';
import {BackHandler} from 'react-native';

interface IBackHandlerProps {
  step: number;
  setStep: Dispatch<SetStateAction<number>>;
  threshold: number;
}

export const useBackHandler = ({
  step,
  setStep,
  threshold,
}: IBackHandlerProps) => {
  const goBack = () => {
    if (step === 0) {
      return false;
    } else {
      setStep(step - 1);
      if (step == threshold && step !== 1) return false;
      return step >= 1 ? true : false;
    }
  };

  useEffect(() => {
    console.log(step);
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      goBack,
    );

    return () => backHandler.remove();
  }, [step]);
};
