import ListMenuOptions from '@components/common/ListMenuOptions';
import {MenuOptionsProps} from '@components/common/MenuOption';
import {useAuth} from '@contexts/authContext';
import {StackScreenProps} from '@react-navigation/stack';
import React from 'react';

interface MainPanelProps extends StackScreenProps<any, any> {}

export const MainPanel: React.FC<MainPanelProps> = ({navigation}) => {
  const {authDispatch} = useAuth();

  const mainMenu: MenuOptionsProps[] = [
    {
      title: 'Citas Médicas',
      imageSrc: 'calendar',
      action: () => navigation.navigate('MedicalAppointmentsPanel'),
    },
    {
      title: 'Recetas Médicas',
      imageSrc: 'syringe',
      action: () => navigation.navigate('MedicalReceiptPanel'),
    },
    {
      title: 'Cerrar Sesión',
      imageSrc: 'exit',
      action: () => authDispatch({type: 'SIGN_OUT'}),
    },
  ];

  return <ListMenuOptions options={mainMenu} cols={2} />;
};
