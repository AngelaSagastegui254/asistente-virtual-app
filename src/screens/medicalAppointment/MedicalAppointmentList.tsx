import {SummaryCardAppointment} from '@components/common/SummaryCardAppointment';
import {useAuth} from '@contexts/authContext';
import {BotType} from '@models/enums/botType';
import {FormatType} from '@models/enums/formatType';
import {MedicalAppointment} from '@models/medicalAppointment';
import {useFocusEffect} from '@react-navigation/native';
import {StackScreenProps} from '@react-navigation/stack';
import {interactWithBot} from '@services/lexService';
import React, {useCallback, useRef, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import Sound from 'react-native-sound';

interface MedicalAppointmentRegisteredProps
  extends StackScreenProps<any, any> {}

export const MedicalAppointmentList: React.FC<MedicalAppointmentRegisteredProps> =
  ({navigation}) => {
    const sound = useRef<Sound | null>(null);
    const {authState} = useAuth();
    const [list, setList] = useState<MedicalAppointment[]>([]);

    const playAudio = (url?: string) => {
      sound.current = new Sound(url, undefined, error => {
        if (error) {
          console.log('failed to load the sound', error);
          return;
        }
        // loaded successfully
        if (sound) {
          sound.current?.play();
        }
      });
    };

    const handleAudio = async (url: string) => {
      let result = await interactWithBot(
        url,
        FormatType.AUDIO,
        FormatType.AUDIO,
        BotType.MEDICAL_APPOINTMENT,
        authState.lexSession,
      );

      console.log(result);

      if (result) {
        const userId = result.textOutput[0]
          .trim()
          .toLowerCase()
          .includes(' id ');

        console.log(userId);

        if (userId && authState.userId) {
          result = await interactWithBot(
            authState.userId.toString(),
            FormatType.AUDIO,
            FormatType.TEXT,
            BotType.MEDICAL_APPOINTMENT,
            authState.lexSession,
          );
          playAudio(result?.audioUrl);
        } else {
          playAudio(result.audioUrl);
        }
      }
    };

    useFocusEffect(
      useCallback(() => {
        const init = async () => {
          try {
            if (!authState.userId) {
              return;
            }

            await interactWithBot(
              'listar citas',
              FormatType.TEXT,
              FormatType.TEXT,
              BotType.MEDICAL_APPOINTMENT,
              authState.lexSession,
            );
            const result = await interactWithBot(
              authState.userId.toString(),
              FormatType.AUDIO,
              FormatType.TEXT,
              BotType.MEDICAL_APPOINTMENT,
              authState.lexSession,
            );

            const appointmentAux = result?.textOutput[0];
            const appointments = appointmentAux.split('.')[1];

            if (appointments) {
              const appointmentsListAux = appointments.split(',');
              const appointmentsList = appointmentsListAux.map(e => {
                const dataAux = e.trim().split(' ');
                const data: MedicalAppointment = {
                  id: Number.parseInt(dataAux[0], 10),
                  specialty: dataAux[1],
                  date: dataAux[2],
                };
                return data;
              });

              setList(appointmentsList);
            }

            sound.current = new Sound(result.audioUrl, undefined, error => {
              if (error) {
                console.log('failed to load the sound', error);
                return;
              }
              // loaded successfully
              if (sound) {
                sound.current?.play();
              }
            });
          } catch (e) {
            console.log(e);
          }
        };

        init();

        return () => {
          sound.current?.stop();
        };
      }, []),
    );

    return (
      <View style={styles.root}>
        <SummaryCardAppointment
          imageSrc="kit"
          title="Lista de citas Médicas Registradas"
          tableEntries={list}
          onInput={() => {
            sound.current?.stop();
          }}
          onHandleInput={handleAudio}
        />
      </View>
    );
  };

const styles = StyleSheet.create({
  root: {
    flex: 1,
    alignContent: 'center',
    justifyContent: 'center',
  },
});
