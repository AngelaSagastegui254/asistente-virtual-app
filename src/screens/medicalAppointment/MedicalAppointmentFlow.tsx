import React, {useRef, useState} from 'react';
import {BackHandler} from 'react-native';
import {StackScreenProps} from '@react-navigation/stack';
import {FormatType} from '@models/enums/formatType';
import {BotType} from '@models/enums/botType';
import {interactWithBot} from '@services/lexService';
import Sound from 'react-native-sound';
import {useAuth} from '@contexts/authContext';
import MicInput from '@components/common/MicInput';
import {useFocusEffect} from '@react-navigation/native';

interface MedicalAppointmentFlowProps extends StackScreenProps<any, any> {}

export const MedicalAppointmentFlow: React.FC<MedicalAppointmentFlowProps> = ({
  navigation,
}) => {
  const {authState, authDispatch} = useAuth();
  const [text, setText] = useState('');
  const sound = useRef<Sound | null>(null);

  useFocusEffect(
    React.useCallback(() => {
      const init = async () => {
        try {
          if (!authState.userId) {
            return;
          }

          await interactWithBot(
            'registrar',
            FormatType.AUDIO,
            FormatType.TEXT,
            BotType.MEDICAL_APPOINTMENT,
            authState.lexSession,
          );

          const response = await interactWithBot(
            authState.userId.toString(),
            FormatType.AUDIO,
            FormatType.TEXT,
            BotType.MEDICAL_APPOINTMENT,
            authState.lexSession,
          );

          sound.current = new Sound(response.audioUrl, undefined, error => {
            if (error) {
              console.log('failed to load the sound', error);
              return;
            }
            // loaded successfully
            if (sound) {
              sound.current?.play();
            }
          });
          setText(response.textOutput[0]);
        } catch (e) {
          console.log(e);
        }
      };
      init();

      const onBackPress = () => {
        authDispatch({type: 'NEW_SESSION'});
        sound.current?.stop();
        return false;
      };
      BackHandler.addEventListener('hardwareBackPress', onBackPress);
      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, []),
  );

  const handleAudio = async (url: string) => {
    const response = await interactWithBot(
      url,
      FormatType.AUDIO,
      FormatType.AUDIO,
      BotType.MEDICAL_APPOINTMENT,
      authState.lexSession,
    );

    if (response) {
      const aux = response.textOutput[0].toLowerCase();

      if (aux.includes('reservada')) {
        setTimeout(() => {
          navigation.reset({
            index: 1,
            routes: [
              // redirect to medical appointment panel
              {name: 'MainPanel'},
              {name: 'MedicalAppointmentsPanel'},
            ],
          });
        }, 5000);
      }

      sound.current = new Sound(response.audioUrl, undefined, error => {
        if (error) {
          console.log('failed to load the sound', error);
          return;
        }
        // loaded successfully
        if (sound) {
          sound.current?.play();
        }
      });

      setText(response.textOutput[0]);
    }
  };

  return (
    <MicInput
      text={text}
      onPress={() => {
        sound.current?.stop();
      }}
      handleAudio={handleAudio}
    />
  );
};
