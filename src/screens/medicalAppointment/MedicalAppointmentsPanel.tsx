import ListMenuOptions from '@components/common/ListMenuOptions';
import {MenuOptionsProps} from '@components/common/MenuOption';
import {StackScreenProps} from '@react-navigation/stack';
import React from 'react';

interface MedicalAppointmentsPanelProps extends StackScreenProps<any, any> {}

export const MedicalAppointmentsPanel: React.FC<MedicalAppointmentsPanelProps> =
  ({navigation}) => {
    const mainMenu: MenuOptionsProps[] = [
      {
        title: 'Reservar Citas Médicas',
        imageSrc: 'band-aid',
        action: () => navigation.navigate('MedicalAppointmentFlow'),
      },
      {
        title: 'Citas Médicas Registradas',
        imageSrc: 'kit',
        action: () => navigation.navigate('MedicalAppointmentList'),
      },
      {
        title: 'Médicos Encargados',
        imageSrc: 'doctor',
        action: () => navigation.navigate('DocsInChargeFlow'),
      },
      {
        title: 'Cuestionario De Salud',
        imageSrc: 'questionnaire',
      },
    ];

    return <ListMenuOptions options={mainMenu} cols={2} />;
  };
