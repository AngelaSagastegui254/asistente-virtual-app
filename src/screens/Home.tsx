import React, {useEffect, useRef} from 'react';
import {StackScreenProps} from '@react-navigation/stack';
import {
  StyleSheet,
  Text,
  ToastAndroid,
  TouchableOpacity,
  View,
} from 'react-native';
import {colores, appTheme} from '@theme/appTheme';
import {interactWithBot} from '@services/lexService';
import {useAuth} from '@contexts/authContext';
import Sound from 'react-native-sound';
import {FormatType} from '@models/enums/formatType';
import {BotType} from '@models/enums/botType';

interface HomeProps extends StackScreenProps<any, any> {}

export const Home: React.FC<HomeProps> = ({navigation}) => {
  const {authState} = useAuth();
  const sound = useRef<Sound | null>(null);
  
  useEffect(() => {
    const init = async () => {
      try {
        const response = await interactWithBot(
          'hola',
          FormatType.AUDIO,
          FormatType.TEXT,
          BotType.AUTH,
          authState.lexSession,
        );

        if (response) {
          sound.current = new Sound(response.audioUrl, undefined, error => {
            if (error) {
              console.log('failed to load the sound', error);
              return;
            }
            // loaded successfully
            if (sound) {
              sound.current?.play();
            }
          });
        }
      } catch (e) {
        //ToastAndroid.showWithGravity(e, ToastAndroid.LONG, ToastAndroid.BOTTOM);
        console.log("Error CTM");
      }
    };

    init();
  }, []);

  const register = () => {
    sound.current?.stop();
    navigation.navigate('SignUp');
  };
  const login = () => {
    sound.current?.stop();
    navigation.navigate('SignIn');
  };

  return (
    <View style={styles.root}>
      <View style={styles.flexRow}>
        <TouchableOpacity style={styles.actionButton} onPress={register}>
          <Text style={appTheme.buttonText}>REGISTRARSE</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.actionButton} onPress={login}>
          <Text style={appTheme.buttonText}>INICIAR SESIÓN</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
  },
  flexRow: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginBottom: 10,
  },
  actionButton: {
    marginTop: 10,
    paddingHorizontal: '5%',
    paddingVertical: '57%',
    backgroundColor: colores.primary,
    borderRadius: 10,
  },
});
