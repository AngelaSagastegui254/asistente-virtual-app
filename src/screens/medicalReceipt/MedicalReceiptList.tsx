import {SummaryCardAppointment} from '@components/common/SummaryCardAppointment';
import {StackScreenProps} from '@react-navigation/stack';
import React, {useRef, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {interactWithBot} from '@services/lexService';
import {FormatType} from '@models/enums/formatType';
import {BotType} from '@models/enums/botType';
import Sound from 'react-native-sound';
import {useFocusEffect} from '@react-navigation/core';
import {useAuth} from '@contexts/authContext';
import {MedicalAppointment} from '@models/medicalAppointment';

interface MedicalReceiptListProps extends StackScreenProps<any, any> {}

export const MedicalReceiptList: React.FC<MedicalReceiptListProps> = ({
  navigation,
}) => {
  console.log("Listaaaaaaaaa");
  const sound = useRef<Sound | null>(null);
  const {authState} = useAuth();

  console.log("aqui");
  const [list, setList] = useState<MedicalAppointment[]>([]);
  console.log("aqui2");
  const playAudio = (url?: string) => {
    console.log("aqui3");
    sound.current = new Sound(url, undefined, error => {
      if (error) {
        console.log('failed to load the sound', error);
        return;
      }
      // loaded successfully
      console.log("aqui4");
      if (sound) {
        sound.current?.play();
        console.log("aqui5");
      }
    });
  };

  useFocusEffect(
    React.useCallback(() => {
      const init = async () => {
        if (!authState.userId) {
          return;
        }
        console.log("Comenzando....");
        try {
          console.log("1....");
          await interactWithBot(
            'recetas',
            FormatType.TEXT,
            FormatType.TEXT,
            BotType.MEDICAL_RECEIPT,
            authState.lexSession,
          );
          console.log("2....");
          const result = await interactWithBot(
            authState.userId.toString(),
            FormatType.AUDIO,
            FormatType.TEXT,
            BotType.MEDICAL_RECEIPT,
            authState.lexSession,
          );
          console.log("3....");
          console.log("resultadoo");
          console.log(result);
          const receiptsAux = result?.textOutput[0];
          const receipts = receiptsAux.split('.')[1];

          if (receipts) {
            console.log("*******");
            console.log(receipts);
            const receiptsListAux = receipts.split(',');
            const receiptsList = receiptsListAux.map(e => {
              const dataAux = e.trim().split(' ');
              const data: MedicalAppointment = {
                id: Number.parseInt(dataAux[0], 10),
                specialty: dataAux[1],
                date: dataAux[2],
              };
              return data;
            });
            setList(receiptsList);
          }
          playAudio(result.audioUrl);
        } catch (e) {
          console.log(e);
        }
      };
      init();

      return () => {
        sound.current?.stop();
      };
    }, []),
  );

  const handleAudio = async (url: string) => {
    console.log("handleAudio");
    console.log("1-------");
    if (!authState.userId) {
      return;
    }
    console.log("2-------");
    try {
      let response = await interactWithBot(
        url,
        FormatType.AUDIO,
        FormatType.AUDIO,
        BotType.MEDICAL_RECEIPT,
        authState.lexSession,
      );
      console.log("3-------");
      const userId = response.textOutput[0]
        .trim()
        .toLowerCase()
        .includes(' id ');
      console.log("4-------");
      if (userId && authState.userId) {
        response = await interactWithBot(
          authState.userId.toString(),
          FormatType.AUDIO,
          FormatType.TEXT,
          BotType.MEDICAL_RECEIPT,
          authState.lexSession,
        );
        playAudio(response?.audioUrl);
      }
      console.log("handleAudio");
      const aux = response.textOutput[0].split('.');

      if (aux.length <= 1) {
        playAudio(response?.audioUrl);
      } else {
        navigation.push('MedicalReceiptDetail', {
          receiptIndexVoiceInputUrl: url,
        });
      }
    } catch (e) {
      console.error(e);
    }
  };
  

  return (
    <View style={styles.root}>
      <SummaryCardAppointment
        imageSrc="band-aid"
        title="Lista de recetas Médicas"
        tableEntries={list}
        onInput={() => {
          sound.current?.stop();
        }}
        onHandleInput={handleAudio}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  bottom: {
    marginTop: 30,
  },
});
