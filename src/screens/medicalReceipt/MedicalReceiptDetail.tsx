import {StackScreenProps} from '@react-navigation/stack';
import React, {useRef, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {SummaryCardDetail} from '@components/common/SymmaryCardDetail';
import {useFocusEffect} from '@react-navigation/core';
import {interactWithBot} from '@services/lexService';
import {FormatType} from '@models/enums/formatType';
import {BotType} from '@models/enums/botType';
import {useAuth} from '@contexts/authContext';
import Sound from 'react-native-sound';
import {ReceiptDetail} from '@models/receiptDetail';

interface MedicalReceiptDetailProps extends StackScreenProps<any, any> {}

export const MedicalReceiptDetail: React.FC<MedicalReceiptDetailProps> = ({
  route,
  navigation,
}) => {
  const {receiptIndexVoiceInputUrl}: any = route.params;
  const {authState} = useAuth();
  const sound = useRef<Sound | null>(null);
  const [tableEntries, setTableEntries] = useState<ReceiptDetail[]>([]);

  useFocusEffect(
    React.useCallback(() => {
      const init = async () => {
        if (!authState.userId) {
          return;
        }

        await interactWithBot(
          'detalles',
          FormatType.TEXT,
          FormatType.TEXT,
          BotType.MEDICAL_RECEIPT,
          authState.lexSession,
        );

        await interactWithBot(
          authState.userId.toString(),
          FormatType.TEXT,
          FormatType.TEXT,
          BotType.MEDICAL_RECEIPT,
          authState.lexSession,
        );

        const response = await interactWithBot(
          receiptIndexVoiceInputUrl,
          FormatType.AUDIO,
          FormatType.AUDIO,
          BotType.MEDICAL_RECEIPT,
          authState.lexSession,
        );

        if (response) {
          const auxDetails = response.textOutput[0].split('.');
          const details = auxDetails[1].split(',');

          const detailsList = details.map((e: string, index: number) => {
            const value: ReceiptDetail = {
              id: index,
              details: e.trim(),
            };
            return value;
          });

          if (detailsList.length > 0) {
            setTableEntries(detailsList);
          }

          sound.current = new Sound(response.audioUrl, undefined, error => {
            if (error) {
              console.log('failed to load the sound', error);
              return;
            }
            // loaded successfully
            if (sound) {
              sound.current?.play();
            }
          });
        }
      };

      init();

      return () => {
        sound.current?.stop();
      };
    }, []),
  );

  return (
    <View style={styles.root}>
      <SummaryCardDetail
        imageSrc="band-aid"
        title="Detalle de la receta"
        tableEntries={tableEntries}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
