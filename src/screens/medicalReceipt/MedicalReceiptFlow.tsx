import React, {useEffect, useState} from 'react';
import {View} from 'react-native';

import {TextCue} from '@components/common/TextCue';
import {useBackHandler} from '@hooks/useBackHandler';
import {StackScreenProps} from '@react-navigation/stack';
import {appTheme} from '@theme/appTheme';

interface MedicalReceiptFlowProps extends StackScreenProps<any, any> {}

const MedicalReceiptFlowFlow = [
  'Ingrese la cantidad y el nombre de la medicina para la orden',
  'Desea agregar otro pedido?',
  'Ingrese la cantidad y el nombre de la medicina para la orden',
  'Su pedido ha sido generado.',
];

export const MedicalReceiptFlow: React.FC<MedicalReceiptFlowProps> = ({
  navigation,
}) => {
  const [step, setStep] = useState(0);

  useBackHandler({step, setStep, threshold: MedicalReceiptFlowFlow.length});

  const handleClick = () => {
    if (step !== MedicalReceiptFlowFlow.length - 1) {
      setStep(step + 1);
    }
  };

  useEffect(() => {
    const timer = setTimeout(() => {
      if (step === MedicalReceiptFlowFlow.length - 1) {
        navigation.reset({
          index: 0,
          routes: [
            // redirect to medical appointment panel
            {name: 'MainPanel'},
          ],
        });
      }
    }, 3000);

    return () => clearTimeout(timer);
  }, []);

  return (
    <View style={appTheme.container}>
      {step >= 0 && step <= MedicalReceiptFlowFlow.length - 1 && (
        <View style={{marginBottom: -100}}>
          <TextCue text={MedicalReceiptFlowFlow[step]} />
        </View>
      )}
    </View>
  );
};
