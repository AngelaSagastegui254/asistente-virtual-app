import ListMenuOptions from '@components/common/ListMenuOptions';
import {MenuOptionsProps} from '@components/common/MenuOption';
import {StackScreenProps} from '@react-navigation/stack';
import React from 'react';

interface MedicalReceiptPanelProps extends StackScreenProps<any, any> {}

export const MedicalReceiptPanel: React.FC<MedicalReceiptPanelProps> = ({
  navigation,
}) => {
  const mainMenu: MenuOptionsProps[] = [
    {
      title: 'Lista de recetas médicas',
      imageSrc: 'band-aid',
      action: () => navigation.navigate('MedicalReceiptList'),
    },
    {
      title: 'Pedidos de compra',
      imageSrc: 'kit',
      action: () => navigation.navigate('MedicalReceiptFlow'),
    },
  ];
  return <ListMenuOptions options={mainMenu} cols={2} />;
};
