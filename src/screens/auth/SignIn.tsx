import React, {useRef, useState} from 'react';
import {BackHandler} from 'react-native';

import {StackScreenProps} from '@react-navigation/stack';
import {useFocusEffect} from '@react-navigation/native';
import {useAuth} from '@contexts/authContext';
import {interactWithBot} from '@services/lexService';
import Sound from 'react-native-sound';
import {FormatType} from '@models/enums/formatType';
import {BotType} from '@models/enums/botType';
import MicInput from '@components/common/MicInput';

interface SignInProps extends StackScreenProps<any, any> {}

export const SignIn: React.FC<SignInProps> = ({navigation}) => {
  const {authState, authDispatch} = useAuth();
  const [text, setText] = useState('');
  const sound = useRef<Sound | null>(null);

  const [disable, setDisable] = useState(false);

  useFocusEffect(
    React.useCallback(() => {
      const init = async () => {
        try {
          const response = await interactWithBot(
            'inicio',
            FormatType.AUDIO,
            FormatType.TEXT,
            BotType.AUTH,
            authState.lexSession,
          );

          sound.current = new Sound(response.audioUrl, undefined, error => {
            if (error) {
              console.log('failed to load the sound', error);
              return;
            }
            // loaded successfully
            if (sound) {
              sound.current?.play();
            }
          });
          setText(response.textOutput[0]);
        } catch (e) {
          console.log(e);
        }
      };
      init();

      const onBackPress = () => {
        authDispatch({type: 'NEW_SESSION'});
        sound.current?.stop();
        navigation.reset({
          index: 0,
          routes: [{name: 'Home'}],
        });
        return true;
      };
      BackHandler.addEventListener('hardwareBackPress', onBackPress);
      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, []),
  );

  const handleAudio = async (url: string) => {
    try {
      console.log("Audio escuchando...");
      console.log(url);
      const response = await interactWithBot(
        url,
        FormatType.AUDIO,
        FormatType.AUDIO,
        BotType.AUTH,
        authState.lexSession,
      );
      
      console.log("response");
      console.log(response);
      sound.current = new Sound(response.audioUrl, undefined, error => {
        if (error) {
          console.log('failed to load the sound', error);
          return;
        }
        // loaded successfully
        if (sound) {
          sound.current?.play();
        }
      });

      setText(response.textOutput[0]);
    } catch (e) {
      console.log("Error");
      console.log(e);
      console.log(".........");
    }
  };

  return (
    <MicInput
      text={text}
      onPress={() => {
        sound.current?.stop();
      }}
      handleAudio={handleAudio}
      disable={disable}
    />
  );
};
