import React, {useState} from 'react';
import {View} from 'react-native';

import {Mic} from '@components/common/Mic';
import {TextCue} from '@components/common/TextCue';
import {useBackHandler} from '@hooks/useBackHandler';
import {StackScreenProps} from '@react-navigation/stack';
import {appTheme} from '@theme/appTheme';

interface DocsInChargeFlowProps extends StackScreenProps<any, any> {}

const DocsInChargeFlowFlow = [
  'Ingrese la especialidad de la que desea consultar',
];

export const DocsInChargeFlow: React.FC<DocsInChargeFlowProps> = ({
  navigation,
}) => {
  const [step, setStep] = useState(0);

  useBackHandler({step, setStep, threshold: DocsInChargeFlowFlow.length});

  const handleClick = () => {
    setStep(step + 1);
    if (step == DocsInChargeFlowFlow.length - 1) {
      navigation.replace('DocsInChargeList');
    }
  };

  return (
    <View style={appTheme.container}>
      <Mic onPress={handleClick} />
      {step >= 0 && step <= DocsInChargeFlowFlow.length - 1 && (
        <View style={{marginBottom: -50}}>
          <TextCue text={DocsInChargeFlowFlow[step]} />
        </View>
      )}
    </View>
  );
};
