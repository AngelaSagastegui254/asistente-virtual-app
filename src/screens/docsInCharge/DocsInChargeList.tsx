import { SummaryCard } from '@components/common/SummaryCardAppointment';
import { StackScreenProps } from '@react-navigation/stack';
import { appTheme } from '@theme/appTheme';
import React from 'react';
import { View } from 'react-native';


interface DocsInChargeListProps
  extends StackScreenProps<any, any> {}

const doctors = [
  'Pedro Suazer Quiñones',
  'Andrea Sánches Aybar',
  'Juana Gonzales Peralta',
  'Susana Zavala Sillones',
];

export const DocsInChargeList: React.FC<DocsInChargeListProps> =
  ({navigation}) => {
    return (
      <View style={appTheme.container}>
        <SummaryCard
          imageSrc="doctor"
          title="Lista de médicos encargados"
          doctors={doctors}
        />
      </View>
    );
  };
