import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Home} from '@screens/Home';
import {SignIn} from '@screens/auth/SignIn';
import {SignUp} from '@screens/auth/SignUp';
import {MainPanel} from '@screens/MainPanel';
import {MedicalAppointmentsPanel} from '@screens/medicalAppointment/MedicalAppointmentsPanel';
import {MedicalAppointmentFlow} from '@screens/medicalAppointment/MedicalAppointmentFlow';
import {MedicalAppointmentList} from '@screens/medicalAppointment/MedicalAppointmentList';
import {DocsInChargeFlow} from '@screens/docsInCharge/DocsInChargeFlow';
import {DocsInChargeList} from '@screens/docsInCharge/DocsInChargeList';
import {MedicalReceiptPanel} from '@screens/medicalReceipt/MedicalReceiptPanel';
import {MedicalReceiptFlow} from '@screens/medicalReceipt/MedicalReceiptFlow';
import {MedicalReceiptList} from '@screens/medicalReceipt/MedicalReceiptList';
import {MedicalReceiptDetail} from '@screens/medicalReceipt/MedicalReceiptDetail';
import {useAuth} from '@contexts/authContext';

export type StackNavigationProps = {};

const Stack = createStackNavigator();

export const StackNavigation: React.FC<StackNavigationProps> = () => {
  const {authState} = useAuth();

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      {!authState.authenticated ? (
        <>
          <Stack.Screen name="Home" component={Home} />
          <Stack.Screen name="SignIn" component={SignIn} />
          <Stack.Screen name="SignUp" component={SignUp} />
        </>
      ) : (
        <>
          <Stack.Screen name="MainPanel" component={MainPanel} />
          <Stack.Screen
            name="MedicalAppointmentsPanel"
            component={MedicalAppointmentsPanel}
          />
          <Stack.Screen
            name="MedicalAppointmentFlow"
            component={MedicalAppointmentFlow}
          />
          <Stack.Screen
            name="MedicalAppointmentList"
            component={MedicalAppointmentList}
          />
          <Stack.Screen name="DocsInChargeFlow" component={DocsInChargeFlow} />
          <Stack.Screen name="DocsInChargeList" component={DocsInChargeList} />
          <Stack.Screen
            name="MedicalReceiptPanel"
            component={MedicalReceiptPanel}
          />
          <Stack.Screen
            name="MedicalReceiptFlow"
            component={MedicalReceiptFlow}
          />
          <Stack.Screen
            name="MedicalReceiptList"
            component={MedicalReceiptList}
          />
          <Stack.Screen
            name="MedicalReceiptDetail"
            component={MedicalReceiptDetail}
          />
        </>
      )}
    </Stack.Navigator>
  );
};
