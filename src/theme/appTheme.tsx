import {StyleSheet} from 'react-native';

export const colores = {
  primary: '#21509C',
};

export const appTheme = StyleSheet.create({
  container: {
    flex: 1,
  },
  bg: {
    flex: 1,
    justifyContent: 'space-evenly',
    alignContent: 'space-between',
  },
  logo: {
    position: 'absolute',
    top: 30,
    left: '12%',
    right: 0,
  },
  buttonText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
    textTransform: 'uppercase',
  },
  buttonOptions: {
    fontSize: 25,
    width: '60%',
  },
  micImage: {
    height: 200,
    width: 200,
  },
  textCueBg: {
    backgroundColor: colores.primary,
    borderRadius: 5,
    padding: 20,
    marginHorizontal: 40,
    marginVertical: 10,
  },
  sumCard: {
    flex: 1,
    width: '100%',
    marginVertical: '40%',
    backgroundColor: colores.primary,
  },
  sumCardContentBg: {
    backgroundColor: 'white',
    marginHorizontal: 20,
  },
  flexRow: {
    paddingVertical: '8%',
    marginTop: '8%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: colores.primary,
  },
  flexTable: {
    paddingVertical: '3%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  flexTableCenter: {
    paddingVertical: '3%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  textCenter: {
    textAlign: 'center',
    fontSize: 10,
  },
});
