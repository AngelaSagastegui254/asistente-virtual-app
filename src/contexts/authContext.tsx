import React from 'react';
import {createContext, useReducer} from 'react';
import uuid from 'react-native-uuid';

type Action = {
  type: 'SIGN_OUT' | 'NEW_SESSION' | 'SIGN_IN';
  payload?: any;
};

type Dispatch = (action: Action) => void;

type State = {
  lexSession: string;
  authenticated: boolean;
  token: string;
  userId?: number;
};

type AuthProviderProps = {children: React.ReactNode};

const initialState: State = {
  lexSession: uuid.v4().toString(),
  authenticated: true,
  token: '',
  userId: 3,
};

const AuthContext = createContext<
  {authState: State; authDispatch: Dispatch} | undefined
>(undefined);

function authReducer(state: State, action: Action): State {
  console.log("-----------");
  console.log("authReducer");
  switch (action.type) {
    case 'NEW_SESSION':
      console.log("NUEEEEVAAAAA SESION")
      return {
        lexSession: uuid.v4().toString(),
        authenticated: state.authenticated,
        userId: state.userId,
        token: state.token,
      };
    case 'SIGN_IN':
      console.log("SIGNNNNNNNN INNNN")
      return {
        lexSession: state.lexSession,
        authenticated: true,
        userId: action.payload.userId,
        token: action.payload.token,
      };
    case 'SIGN_OUT':
      return {
        lexSession: state.lexSession,
        authenticated: false,
        token: '',
      };
    default:
      return initialState;
  }
}

const AuthProvider = ({children}: AuthProviderProps) => {
  console.log("AuthProvider");
  const [state, dispatch] = useReducer(authReducer, initialState);
  const value = {authState: state, authDispatch: dispatch};
  console.log(value);
  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};

function useAuth() {

  console.log(AuthContext);
  const context = React.useContext(AuthContext);
  console.log(context);
  if (context === undefined) {
    throw new Error('useAuth must be used within a AuthProvider');
  }
  return context;
}

export {AuthProvider, useAuth};
