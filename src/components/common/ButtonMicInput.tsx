import {useAudioRecorder} from '@hooks/useAudioRecorder';
import React, {useState} from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

interface ButtonMicInputProps {
  handleAudio: (url: string) => void;
  onInput?: () => void;
  style: {};
  disable?: boolean;
  size?: number;
}

const styles = StyleSheet.create({
  root: {
    alignSelf: 'baseline',
  },
  button: {
    borderRadius: 100,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2C8AAB',
  },
});

function ButtonMicInput(props: ButtonMicInputProps) {
  const [recording, setRecording] = useState(false);
  const {startRecord, stopRecord} = useAudioRecorder();

  const handleRecord = async () => {
    if (!recording) {
      props.onInput?.();

      startRecord('audio_user.wav');
      setRecording(true);
    } else {
      const url = await stopRecord();
      setRecording(false);
      props.handleAudio(url);
    }
  };

  const disable = props.disable || false;

  const iconSize = props.size || 20;
  const buttonSize = iconSize * 2;

  return (
    <View style={[styles.root, props.style]}>
      <TouchableOpacity
        style={{...styles.button, height: buttonSize, width: buttonSize}}
        onPress={handleRecord}
        disabled={disable}>
        <Icon name="microphone" size={iconSize} color="white" solid />
      </TouchableOpacity>
    </View>
  );
}

export default ButtonMicInput;
