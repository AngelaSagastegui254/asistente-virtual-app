import {colores} from '@theme/appTheme';
import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {ImageAsset} from '../../types/image.type';
import {requireConstructor} from '../../utils/requireConstructor';

export interface MenuOptionsProps {
  imageSrc: ImageAsset;
  title: string;
  action?: () => void;
  style?: {};
}

export const MenuOptions: React.FC<MenuOptionsProps> = ({
  imageSrc,
  title,
  style,
  action,
}) => {
  let logo = requireConstructor(imageSrc);

  return (
    <TouchableOpacity style={[styles.root, style]} onPress={action}>
      <Image source={logo as any} style={styles.img} />
      <Text style={styles.title}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  img: {
    height: 100,
    width: 100,
    marginVertical: 5,
  },
  root: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colores.primary,
    borderRadius: 5,
  },
  title: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
    textTransform: 'uppercase',
    width: '60%',
  },
});
