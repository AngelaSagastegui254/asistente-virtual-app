import React from 'react';
import {View, StyleSheet} from 'react-native';
import {MenuOptions, MenuOptionsProps} from './MenuOption';

interface ListMenuOptionsProps {
  options: MenuOptionsProps[];
  cols?: number;
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignContent: 'center',
  },
  item: {
    margin: '1%',
  },
});

function ListMenuOptions({options, cols = 1}: ListMenuOptionsProps) {
  const itemWidth = (1 / cols - 0.02) * 100;
  const itemHeight = (1 / Math.ceil(options.length / cols) - 0.02) * 100;
  const completeItems = Math.floor(options.length / cols) * cols;
  const incompleteItems = options.length - completeItems;
  const incompleteItemsExtraWidth =
    (100 - itemWidth * incompleteItems) / incompleteItems - 2;

  return (
    <View style={styles.root}>
      {options.map((option, index) => {
        if (index >= options.length - incompleteItems) {
          return (
            <MenuOptions
              key={option.title}
              imageSrc={option.imageSrc}
              title={option.title}
              action={option.action}
              style={{
                ...styles.item,
                height: `${itemHeight}%`,
                width: `${itemWidth + incompleteItemsExtraWidth}%`,
              }}
            />
          );
        }

        return (
          <MenuOptions
            key={option.title}
            imageSrc={option.imageSrc}
            title={option.title}
            action={option.action}
            style={{
              ...styles.item,
              height: `${itemHeight}%`,
              width: `${itemWidth}%`,
            }}
          />
        );
      })}
    </View>
  );
}

export default ListMenuOptions;
