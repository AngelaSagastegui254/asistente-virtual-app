import React from 'react';
import {Text, View} from 'react-native';
import {appTheme} from '@theme/appTheme';

interface TextCueProps {
  text: string;
}

export const TextCue: React.FC<TextCueProps> = ({text}) => {
  return (
    <View style={appTheme.textCueBg}>
      <Text style={appTheme.buttonText}>{text}</Text>
    </View>
  );
};
