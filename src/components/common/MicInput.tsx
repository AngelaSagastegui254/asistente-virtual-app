import {useAudioRecorder} from '@hooks/useAudioRecorder';
import React, {useState} from 'react';
import {View, StyleSheet} from 'react-native';
import {Mic} from './Mic';
import {TextCue} from './TextCue';

interface MicInputProps {
  handleAudio: (url: string) => void;
  onPress?: () => void;
  disable?: boolean;
  password?: boolean;
  text: string;
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 5,
    paddingVertical: 2,
    width: 200,
    textDecorationColor: 'black',
  },
  inputControl: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    marginVertical: 10,
  },

  button: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignContent: 'center',
    marginHorizontal: 5,
  },
});

function MicInput(props: MicInputProps) {
  const [recording, setRecording] = useState(false);

  const {startRecord, stopRecord} = useAudioRecorder();

  const handleRecord = async () => {
    if (!recording) {
      if (props.onPress) {
        props.onPress();
      }
      startRecord('audio_user.wav');
      setRecording(true);
    } else {
      const url = await stopRecord();
      setRecording(false);
      props.handleAudio(url);
    }
  };

  const disable = props.disable || false;

  return (
    <View style={styles.root}>
      <Mic onPress={handleRecord} disable={disable} />
      {props.text ? <TextCue text={props.text} /> : null}
    </View>
  );
}

export default MicInput;
