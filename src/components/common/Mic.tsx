import React from 'react';
import {Image, TouchableOpacity} from 'react-native';
import {appTheme} from '@theme/appTheme';

interface MicProps {
  onPress: () => void;
  disable: boolean;
}

export const Mic: React.FC<MicProps> = ({onPress, disable}) => {
  return (
    <TouchableOpacity onPress={onPress} disabled={disable}>
      <Image
        source={require('@assets/img/mic.png')}
        style={appTheme.micImage}
      />
    </TouchableOpacity>
  );
};
