import {appTheme, colores} from '@theme/appTheme';
import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {ImageAsset} from '../../types/image.type';
import {requireConstructor} from '@utils/requireConstructor';
import {MedicalAppointment} from '@models/medicalAppointment';
import {FlatList} from 'react-native-gesture-handler';
import ButtonMicInput from './ButtonMicInput';

interface SummaryCardAppointmentProps {
  title: string;
  imageSrc: ImageAsset;
  tableEntries: MedicalAppointment[];
  onHandleInput: (url: string) => void;
  onInput?: () => void;
}

export const SummaryCardAppointment: React.FC<SummaryCardAppointmentProps> = ({
  title,
  imageSrc,
  tableEntries,
  onInput,
  onHandleInput,
}) => {
  let logo = requireConstructor(imageSrc);

  const renderItem = (item: any) => {
    return (
      <View style={styles.listRow}>
        <Text style={styles.listRowItem}>{item.item.specialty}</Text>
        <Text style={{...styles.listRowItem, textAlign: 'center'}}>
          {item.item.date}
        </Text>
      </View>
    );
  };

  return (
    <View style={styles.sumCard}>
      <View style={styles.sumCardTitle}>
        <Image source={logo as any} style={styles.img} />
        <Text style={{...appTheme.buttonText, ...appTheme.buttonOptions}}>
          {title}
        </Text>
      </View>

      <ButtonMicInput
        onInput={onInput}
        handleAudio={onHandleInput}
        style={styles.button}
        disable={false}
        size={50}
      />

      <FlatList
        style={styles.list}
        data={tableEntries}
        renderItem={renderItem}
        keyExtractor={item => item.id.toString()}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  sumCard: {
    height: '80%',
    width: '100%',
    backgroundColor: colores.primary,
  },
  img: {
    height: 100,
    width: 100,
  },
  sumCardTitle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 20,
  },
  button: {
    alignSelf: 'center',
  },
  list: {
    marginVertical: 20,
    marginHorizontal: 10,
    backgroundColor: 'gray',
    borderRadius: 2,
    elevation: 3,
    shadowColor: 'rgba(0,0,0,0.5)',
  },
  listRow: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    paddingVertical: 10,
    marginVertical: 3,
    marginHorizontal: 5,
    elevation: 10,
    shadowColor: 'rgba(0,0,0,0.5)',
    backgroundColor: 'white',
  },
  listRowItem: {
    flex: 1,
    fontSize: 20,
  },
});
