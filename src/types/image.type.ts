export type ImageAsset =
  | 'calendar'
  | 'syringe'
  | 'observations'
  | 'exit'
  | 'band-aid'
  | 'kit'
  | 'doctor'
  | 'questionnaire';
