import {FormatType} from '@models/enums/formatType';
import {BotType} from '@models/enums/botType';
import {api} from '@utils/constants';
import {
  BotInteractionData,
  BotInteractionInput,
  BotInteractionOutput,
} from '@models/botInteraction';
import RNFetchBlob from 'rn-fetch-blob';

import {CognitoIdentityClient} from '@aws-sdk/client-cognito-identity';
import {fromCognitoIdentityPool} from '@aws-sdk/credential-provider-cognito-identity';
import {
  LexRuntimeV2Client,
  RecognizeUtteranceCommand,
  RecognizeUtteranceCommandOutput,
} from '@aws-sdk/client-lex-runtime-v2';
import {Buffer} from 'buffer';
import {
  authBotData,
  BotData,
  botRegion,
  medicalApointmentBotData,
  medicalReceiptBotData,
} from '@utils/botConfig';

const lexClient = new LexRuntimeV2Client({
  region: botRegion,
  credentials: fromCognitoIdentityPool({
    client: new CognitoIdentityClient({region: botRegion}),
    // Replace IDENTITY_POOL_ID with an appropriate Amazon Cognito Identity Pool ID for, such as 'us-east-1:xxxxxx-xxx-4103-9936-b52exxxxfd6'.
    identityPoolId: 'us-east-1:0beff08b-2cc4-4b5a-ba27-f5183eaa0106',
  }),
});

function blobToBase64(blob: Blob): Promise<any> {
  return new Promise((resolve, _) => {
    const reader = new FileReader();
    console.log(reader);
    reader.onloadend = () => resolve(reader.result);
    console.log(reader.onloadend);
    reader.readAsDataURL(blob);
  });
}

function getBotData(botType: BotType): BotData {
  switch (botType) {
    case BotType.AUTH:
      return authBotData;
    case BotType.MEDICAL_APPOINTMENT:
      return medicalApointmentBotData;
    case BotType.MEDICAL_RECEIPT:
      return medicalReceiptBotData;
    default:
      return medicalReceiptBotData;
  }
}



export async function interactWithBot(
  value: string,
  responseType: FormatType,
  requestType: FormatType,
  botType: BotType,
  sessionId: string,
): Promise<BotInteractionData> {
  const botData: BotData = getBotData(botType);
  const requestContentType =
    requestType === FormatType.TEXT
      ? 'text/plain; charset=utf-8'
      : 'audio/x-l16; sample-rate=16000; channel-count=1';

  const responseContentType =
    responseType === FormatType.TEXT
      ? 'text/plain; charset=utf-8'
      : 'audio/mpeg';

  let input: any = value;
  if (requestType === FormatType.AUDIO) {
    try {
      console.log("FormatType.AUDIO");
      const userAudio = await RNFetchBlob.fs.readFile(value, 'base64');
      input = Buffer.from(userAudio.toString(), 'base64');
    } catch (e) {
      console.log(e);
      throw Error('Audio error');
    }
  }
  const command: RecognizeUtteranceCommand = new RecognizeUtteranceCommand({
    inputStream: input,
    requestContentType: requestContentType,
    responseContentType: responseContentType,
    botId: botData?.botId,
    botAliasId: botData?.botAliasId,
    localeId: 'es_ES',
    sessionId: sessionId,
  });

  try {
    const result: RecognizeUtteranceCommandOutput = await lexClient.send(
      command,
    );
    
    console.log("Result 2:");
    console.log(result);
    console.log("RequestType");
    console.log(requestType);
    if (requestType === FormatType.AUDIO) {
      console.log("Audio");
      const inputMessage: BotInteractionInput = {
        value: result.inputTranscript,
      };
      const output: BotInteractionOutput = await fetch(`${api}/api/lex`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(inputMessage),
      }).then(e => e.json());
      console.log("--------------");
      console.log(output);
    }

    let url = '';
    if (responseType === FormatType.AUDIO) {
      console.log("Audio2");
      url = `${RNFetchBlob.fs.dirs.MainBundleDir}/audio_bot.mp3`;
      console.log("URL");
      console.log(url); 
      let audio = await blobToBase64(result.audioStream as Blob);
      console.log("AUDIO******************");
      
      audio = audio.split(',')[1];
      console.log(audio);
      await RNFetchBlob.fs.writeFile(url, audio, 'base64');
    }

    const botInteractionInput: BotInteractionInput = {
      value: result.messages,
    };
    console.log("result.messages");
    console.log(result);
    console.log(`${api}/api/lex`);
    console.log(JSON.stringify(botInteractionInput));
    const botInteractionOutput: BotInteractionOutput = await fetch(
      `${api}/api/lex`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(botInteractionInput),
      },
    ).then(e => e.json());
    console.log("botInteractionOutput");
    console.log(botInteractionOutput);

    let textOutput: string[] = [];
    if (botInteractionOutput.textOutput) {
      console.log(botInteractionOutput.textOutput);
      textOutput = botInteractionOutput.textOutput?.map(e => e.content);
    }
    console.log("textOutput");
    console.log(textOutput);
    console.log(url);
    return {
      textOutput: textOutput,
      audioUrl: url,
    };

  } catch (e) {
    console.error(e);
    throw new Error('Lex service error');
  }
}
