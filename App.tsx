import {NavigationContainer, DefaultTheme} from '@react-navigation/native';
import React, {useEffect, useLayoutEffect} from 'react';
import {Image, ImageBackground} from 'react-native';
import 'react-native-gesture-handler';
import {StackNavigation} from '@navigation/StackNavigation';
import {appTheme} from '@theme/appTheme';
import {AuthProvider} from './src/contexts/authContext';
import 'react-native-get-random-values';
import 'react-native-url-polyfill/auto';
import {requestAudioPermission} from '@utils/permission';

export default function App() {
  useLayoutEffect(() => {
    requestAudioPermission();
  }, []);

  const MyTheme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      background: 'transparent',
    },
  };

  return (
    <AuthProvider>
      <ImageBackground
        source={require('./src/assets/img/bg.jpeg')}
        resizeMode="cover"
        style={appTheme.bg}>
        <Image
          source={require('./src/assets/img/essalud-logo.png')}
          style={appTheme.logo}
        />
        <NavigationContainer theme={MyTheme}>
          <StackNavigation />
        </NavigationContainer>
      </ImageBackground>
    </AuthProvider>
  );
}
